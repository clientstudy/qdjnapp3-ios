//
//  HttpTestVC.swift
//  qdjnapp3
//
//  Created by qdillion on 2017. 6. 23..
//  Copyright © 2017년 qdillion. All rights reserved.
//

import UIKit

class HttpTestVC: QDViewController {
    @IBOutlet var textView: UITextView!
    
    @IBAction func tapHttpTestBtn(_ sender: UIButton) {
        GlobalFunctions.shared.showSimpleAlert("알럿입니다.")
        //************************************************************************************************
        // 팔로우 리스트 모델 사용 예제
        
        // 1. 생성
        let followListModel = FollowListModel()
        
        // 2. 파라미터 설정
        followListModel.setPrameters(parameters:["last_seq": "0",
                                                 "u_token": "7rnAzXAXqHaJzKp3+q83U7Ya3Dl0lwD7Lsj5xGCZTEGzaKZXEbokbvRFsBktFm+aWTD5ALE5x74PiHM1yFS4uTxBN4oX7dojXPIZbhA2gtCczCD1s+mRgcmeSOv3RnYFUWsJnZY11qyIztwEnsHaXLceYoMkwy7cKbkGdJF9wrU=",
                                                 "l_type": "1",
                                                 "p_u_seq": "6",
                                                 "u_seq": "0"])
        // 3. 서버와 통신
        followListModel.httpGet(success: { [weak self] in
            guard let `self` = self else { return }
            // 4. 서버서 가져온 정보 활용
            Log.outDump(followListModel.responses)
            self.textView.text = self.textView.text + followListModel.responses.description + "\n****************************\n\n"
        }) { (error) in
        }

        
        
        
        
        //************************************************************************************************
        // 테마 리스트 모델 사용 예제
        
        // 1. 생성
        let themeListModel = ThemeListModel()
        
        // 2. 서버와 통신
        themeListModel.httpGet(success: {  [weak self] in
            guard let `self` = self else { return }
            // 3. 서버서 가져온 정보 활용
            Log.outDump(themeListModel.responses)
            self.textView.text = self.textView.text + themeListModel.responses.description + "\n****************************\n\n"
        }) { (error) in
        }
        
        
    }
}

//
//  StackViewTestVC.swift
//  qdjnapp3
//
//  Created by qdillion on 2017. 6. 23..
//  Copyright © 2017년 qdillion. All rights reserved.
//

import UIKit

@available(iOS 9.0, *)
class StackViewTestVC: QDViewController {
    @IBOutlet var textView: UITextView!
    @IBOutlet var stackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setDoneButton(textView: textView)
        self.stackView.addArrangedSubview(textView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



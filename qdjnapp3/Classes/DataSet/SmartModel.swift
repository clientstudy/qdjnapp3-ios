//
//  SmartModel.swift
//  qdjnapp
//
//  Created by qdillion on 2017. 9. 26..
//  Copyright © 2017년 qdillion. All rights reserved.
//

import SwiftyJSON
import Alamofire

/**
 dev.joongna.com/json/hidden_market_contents_list.php
 
 input 값
 - page : 현재 페이지(최초로드일 경우 0)
 - first_seq : 맨 처음 콘텐츠 seq (더보기시에만 전달하며, 서버 리턴값 그대로 전송)
 
 리턴값
 - type : hidden_market_contents_list
 - msg : 상황별 메시지
 - code : 00 - 성공
 01 - 리스트 없음 (더보기 시에도 동일. 분기처리 필요)
 02 - 연결실패
 - data : 리스트 존재할 경우 배열 리턴
 seq - contents seq
 banner_file_path - 리스트이미지url
 is_opened - 0 : 마감 / 1 : 오픈
 type - 0 : 내부페이지 (추후추가)
 link_url : 연결될 링크
 - first_seq : 맨처음 콘텐츠 seq (사용자가 새로고침 하지 않을 시 항상 동일)
 
 정렬순서
 - 1순위 : 오픈, 마감
 2순위 : 날짜순
 */

/// 메인-비밀의공구 (page 처리)
final class SmarketModel: NetworkModel {
    //************************************************************************************************
    // MARK:- init (페이지 관리를 위해 필요한 프로퍼티 선언 및 초기화)
    override init() {
        super.init()
        self.reset()
    }
    
    // 페이지 관리
    var page: Int = 0
    var firstSeq: String = "0"
    
    // 초기화
    func reset() {
        self.page = 0       // 페이징 처리
        self.firstSeq = "0" // 페이징 처리
        self.responses.removeAll()
    }
    
    //************************************************************************************************
    // MARK:- URL
    let url = "/json/hidden_market_contents_list.php" // 메인-비밀의공구
    
    //************************************************************************************************
    // MARK:- Model
    var responses: [Response] = [Response]()
    struct Response {
        let imageBaseUrl: String = "http://images.joongna.com/"   // 이미지 base url
        
        var eSeq: String
        var type: String /// 0 : 내부페이지, 0 아니면 외부페이지
        var url: String
        var imageUrl: String
        var isClosed: Bool
        
        init(_ json: JSON) {
            self.eSeq = json["seq"].stringValue
            self.type = json["type"].stringValue    /// 0 : 내부페이지 (추후추가)
            self.url = json["link_url"].stringValue
            self.imageUrl = self.imageBaseUrl + json["banner_file_path"].stringValue
            self.isClosed = "0" == json["is_opened"].stringValue.uppercased() ? true : false    /// is_opened - 0 : 마감 / 1 : 오픈
        }
    }
    
    //************************************************************************************************
    // MARK:- Request
    // TODO:- 나중에 서버에서 REST 적용시 하단의 Methods를 사용 예정
    func httpGet(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
        self.setPrameters(parameters: ["page":String(self.page), "first_seq":self.firstSeq])
        
        // 통신
        self.http(GlobalVariables.shared.baseUrl + self.url,
                  method: .post,
                  isProgress: false,
                  isAutoErrorMessage: false,
                  success: { [weak self] json in
                    guard let `self` = self else { return }
                    self.responses = json.arrayValue.map { Response($0) }
                    if let mainResponse = self.mainResponse, mainResponse.isSuccess {
                        self.page += 1
                        
                        if let json = self.originJson {
                            self.firstSeq = json["first_seq"].stringValue
                        }
                    }
                    success()
        }) { (error) in
            failure(error)
        }
    }
    
    //    // POST(생성)
    //    func httpSet(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
    //
    //    }
    //
    //    // PUT(업데이트)
    //    func httpUpdate(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
    //
    //    }
    //
    //    // DELETE(삭제)
    //    func httpDel(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
    //
    //    }
}

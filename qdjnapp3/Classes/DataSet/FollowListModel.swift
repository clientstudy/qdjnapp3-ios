//
//  FollowListModel.swift
//  QDNetwork
//
//  Created by qdillion_ios on 2017. 3. 9..
//  Copyright © 2017년 qdillion. All rights reserved.
//

import SwiftyJSON
import Alamofire

/// 팔로우 리스트 (responses가 array)
class FollowListModel: NetworkModel {
    //************************************************************************************************
    // MARK:- URL
    let url = "/json/product_user_follow_list.php"
    
    //************************************************************************************************
    // MARK:- Model
    var responses: [Response] = [Response]()
    struct Response {
        let seq: String
        let nickname: String
        
        init(_ json: JSON) {
            self.seq        = json["seq"].stringValue
            self.nickname   = json["p_u_nickname"].stringValue
        }
    }
    
    //************************************************************************************************
    // MARK:- Request
    // TODO:- 나중에 서버에서 REST 적용시 하단의 Methods를 사용 예정
    func httpGet(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
        self.http(GlobalVariables.shared.baseUrl + self.url,
                  method: .post,
                  isProgress: true,
                  isAutoErrorMessage: true,
                  success: { (json) in
                    self.responses = json.arrayValue.map{ Response($0) }
                    success()
        }) { (error) in
            failure(error)
        }
    }
    
//    // POST(생성)
//    func httpSet(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
//        
//    }
//    
//    // PUT(업데이트)
//    func httpUpdate(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
//        
//    }
//    
//    // DELETE(삭제)
//    func httpDel(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
//        
//    }
}


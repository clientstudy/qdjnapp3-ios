//
//  ThemeListModel.swift
//  APIClient
//
//  Created by qdillion on 2017. 6. 20..
//  Copyright © 2017년 toplogic. All rights reserved.
//

import SwiftyJSON
import Alamofire

/// 테마 리스트 (responses가 array)
class ThemeListModel: NetworkModel {
    //************************************************************************************************
    // MARK:- URL
    let url = "/json/theme_list.php"
    
    //************************************************************************************************
    // MARK:- Model
    var responses: [Response] = [Response]()
    struct Response {
        let id: String
        let name: String
        let img: String
        let listImg: String
        let count: Int
        let type: String        /// type:- A: 키워드 B: 실시간 상품
        
        init(_ json: JSON) {
            self.id         = json["theme_id"].stringValue
            self.name       = json["theme_name"].stringValue
            self.img        = json["t_img"].stringValue
            self.listImg    = json["t_list_img"].stringValue
            self.count      = json["t_count"].intValue
            self.type       = json["t_type"].stringValue
        }
    }
    
    //************************************************************************************************
    // MARK:- Request
    // TODO:- 나중에 서버에서 REST 적용시 하단의 Methods를 사용 예정
    func httpGet(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
        self.http(GlobalVariables.shared.baseUrl + self.url,
                  method: .post,
                  isProgress: true,
                  isAutoErrorMessage: true,
                  success: { (json) in
                    self.responses = json.arrayValue.map{ Response($0) }
                    success()
        }) { (error) in
            failure(error)
        }
    }
    
    //    // POST(생성)
    //    func httpSet(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
    //
    //    }
    //
    //    // PUT(업데이트)
    //    func httpUpdate(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
    //
    //    }
    //
    //    // DELETE(삭제)
    //    func httpDel(success:@escaping () -> Void, failure:@escaping (Error?) -> Void) {
    //        
    //    }
}

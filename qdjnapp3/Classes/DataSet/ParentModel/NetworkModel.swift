//
//  NetworkModel.swift
//  APIClient
//
//  Created by qdillion on 2017. 6. 19..
//  Copyright © 2017년 toplogic. All rights reserved.
//

/** Network Model
 서버 통신과 Response를 한곳에서 관리 하는 것을 목표료 설계
 Class 포함 내역
    - 서버 URL
    - 서버로 보낼 Parameter
    - 서버에서 받을 Responce
    - 서버와 통신(http)
 */

import SwiftyJSON
import Alamofire

class NetworkModel: ModelObject {
    fileprivate var parameters: [String: Any]?
    var mainResponse: MainResponse? = nil       /// server에서 내려주는 data처리 성공 유무 response
    var originJson: JSON?                       /// 원본 response json을 복사 저장.
    
    func setPrameters(parameters: [String: Any]?) {
        self.parameters = parameters
    }
    
    struct MainResponse {
        let type: String
        let code: String
        let msg: String
        let data: JSON?
        let isSuccess: Bool
        
        init(json: JSON?) {
            self.type   = json?["type"].stringValue ?? ""
            self.code   = json?["code"].stringValue ?? ""
            self.msg    = json?["msg"].stringValue ?? ""
            self.data   = json?["data"]
            
            // 통신 성공 유무 저장
            if "00" == self.code {
                self.isSuccess = true
            }
            else {
                self.isSuccess = false
            }
        }
    }
    
    /// - parameter isProgress : 프로그래스 사용 유무 -> on:true, off:false
    /// - parameter isAutoErrorMessage : 에러시 자동으로 alert 처리 유무 -> 자동: treu, 수동으로 직접처리 : false
    func http(_ url: String, method: HTTPMethod, isProgress:Bool, isAutoErrorMessage:Bool, success:@escaping (JSON) -> Void, failure:@escaping (Error?) -> Void) {
        GlobalFunctions.shared.request(url,
                                       method: method,
                                       parameters: self.parameters,
                                       isProgress: isProgress,
                                       isAutoErrorMessage: isAutoErrorMessage,
                                       success: { [weak self] (json) in
                                        guard let `self` = self else { return }
                                        
                                        // MainDataSet의 responce 파싱.
                                        self.originJson = json
                                        self.mainResponse = MainResponse(json: json)
                                        
                                        if let data = self.mainResponse?.data {
                                            success(data)
                                        }
                                        else {
                                            Log.out("!!! [\(type(of: self))]의 response 파싱 문제 발생 !!!")
                                            failure(nil)
                                        }
        })
        { (error) in
            failure(error)
        }
    }
}

//
//  GlobalVariables.swift
//  APIClient
//
//  Created by qdillion on 2017. 6. 20..
//  Copyright © 2017년 toplogic. All rights reserved.
//

/*
 iOS 2017.06.20 이창섭 수석 , 전상일 책임
 - 각 데이터에 네트워크 단순화를 위해 부모 클래스로 옮기기 => 옮길수 있는 부분은 처리 완료.
 - 뷰 컨트롤러에서 객체생성과 동시에 파라미터 값 넣어서 셋팅 (통신준비완료 되게) => 인스턴트 생성시 파라미터를 처리 할수 있게 변경함.
 - 로그 스태틱으로 안쓰고 생성해서 쓸수 있는거 데비
 - HUD 네트워크 글로벌에 추가
 - 뷰모델 방법 논의
 버튼이나 뷰 공융 사용을 위한 디자인 팀과 공유
 - 서버쪽에 API어떤식으로 규격화 할껀지 답변
 - 토큰 정보 헤더에 포함시켜서 통신 요망
 - QD클래스를 상속받아서 작업할수 있게
 - 주석처리 킉 헬프에 나올수 있게 작성
 */

/** Codding 스타일 정의
 - optional로 선언된 변수를 사용할경우 강제추출 연산자"!"를 지양하고 "if let"을 이용해 nil체킹을 한후 처리 하기.
 - IBAction의 연결 Method의 경우 이름앞에 "tap"를 붙여 준다.
 - 다른 인스턴스로 이동을 하는 Method의 경운 이름앞에 "go"를 붙여 준다.
 - Quick Help활용을 위해 (///, /** */)주석을 이용 한다. 가능하면 코드 자체로 문서화 할수 있게 처리 하기.
 - 변수 선언시 가능한 type을 직접 지정하기(타잎추론 x)
 - 상속이 필요 없는 class의 경우 가능하면 struct로 이용 하기.(struct를 다른변수에 넣을경우 값을 복사해 처리하고, class는 참조만 넘기는 방식임)
 */

class GlobalVariables {
    //=============================================================================================//
    // MARK:- Singleton 설정
    static let shared = GlobalVariables()
    
    //=============================================================================================//
    // MARK:- 어느 서버인지, 해당 서버에 맞게 설정
    enum WhichServer {
        case dev
        case stage
        case live
    }
    
    #if DEV //데브서버
    let server: WhichServer = .dev
    let baseUrl = "http://dev.joongna.com"
    #elseif STAGE //스테이지 서버
    let server: WhichServer = .stage
    let baseUrl = "https://stage.joongna.com"
    #else //라이브 서버
    let server: WhichServer = .live
    let baseUrl = "https://www.joongna.com"
    #endif
}

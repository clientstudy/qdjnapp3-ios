//
//  QDViewController.swift
//  qdjnapp3
//
//  Created by qdillion on 2017. 6. 22..
//  Copyright © 2017년 qdillion. All rights reserved.
//
import UIKit

/**
 
 */
class QDViewController: UIViewController {
    //=============================================================================================//
    // MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 키보드외의 영역을 터치시 키보드를 내리기 위한 처리
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(QDViewController.tapGesture(recognizer:)))
        tapRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // add 키보드 옵저버
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // remove 키보드 옵저버
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //=============================================================================================//
    // MARK:- 키보드 제어 관련
    
    /// 키보드 활성화 유무 -> 키보드 활성화 : true, 키보드 비활성화 : false
    open var isShowKeyboard = false
    
    /// 키보드 상단에 완료 버튼추가를 위한 클로져
    private let toolBar: (_ target:QDViewController) -> UIToolbar = { target in
        let toolBar = UIToolbar()
        let doneButton = UIBarButtonItem(title: "완료", style: UIBarButtonItemStyle.done, target: target, action: #selector(QDViewController.hideKeyboard))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        return toolBar
    }
    
    /// 키보드 높이값을 가져 온다.
    private let getKeyboardHeight: (_ notification: Notification) -> CGFloat = { notification in
        if let userInfo:NSDictionary = notification.userInfo as NSDictionary? {
            let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
            let keyboardRectangle = keyboardFrame.cgRectValue
            return keyboardRectangle.height
        }
        return 0.0
    }
    
    /// UITextField 키보드 상단에 완료 버튼추가
    open func setDoneButton(textfield: UITextField) {
        textfield.inputAccessoryView = toolBar(self)
    }
    /// UITextView 키보드 상단에 완료 버튼추가
    open func setDoneButton(textView: UITextView) {
        textView.inputAccessoryView = toolBar(self)
    }
    /// 키보드 숨기기 (override를 통해 자식 class에서 직접 처리 해줄수도 있다.)
    open func hideKeyboard() {
        guard isShowKeyboard else {return}
        
        Log.out("<QDViewController> 키보드 숨기기")
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        guard false == self.isShowKeyboard else { return }
        self.isShowKeyboard = true
        
        // 키보드가 높이 만큼 view위치를 이동.
        if self.view is UIScrollView {
            
            let scrollView: UIScrollView = self.view as! UIScrollView
            
            var insets = scrollView.contentInset
            insets.bottom += self.getKeyboardHeight(notification)
            scrollView.contentInset = insets
            
            insets = scrollView.scrollIndicatorInsets
            insets.bottom += self.getKeyboardHeight(notification)
            scrollView.scrollIndicatorInsets = insets
            
            scrollView.contentSize.height = scrollView.subviews.first?.frame.size.height ?? 0
        }
        else {
            self.view.frame.origin.y -= self.getKeyboardHeight(notification)
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        guard true == self.isShowKeyboard else { return }
        self.isShowKeyboard = false
        
        if self.view is UIScrollView {
            let scrollView: UIScrollView = self.view as! UIScrollView
            
            var insets = scrollView.contentInset
            insets.bottom -= self.getKeyboardHeight(notification)
            scrollView.contentInset = insets
            
            insets = scrollView.scrollIndicatorInsets
            insets.bottom -= self.getKeyboardHeight(notification)
            scrollView.scrollIndicatorInsets = insets
        }
        else {
            self.view.frame.origin.y += self.getKeyboardHeight(notification)
        }
    }
    
    //=============================================================================================//
    // MARK:- Tap 제스쳐 제어
    /// 탭 action이 있을때 호출 (백그라운드 tap의 경우 처리, 보통은 키보드가 활성화 되어 있을경우 키보드 내릴때 사용함)
    /// override를 통해 tap시 제어 가능
    open func tapGesture(recognizer: UITapGestureRecognizer) {
        self.hideKeyboard()
    }
}

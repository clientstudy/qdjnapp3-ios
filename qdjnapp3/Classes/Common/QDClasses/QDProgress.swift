//
//  QDProgress.swift
//  qdjnapp3
//
//  Created by qdillion on 2017. 6. 21..
//  Copyright © 2017년 qdillion. All rights reserved.
//

import UIKit

/** progress 관리
 count 관리로 count가 0일경우 멈춤
 */
class QDProgress {
    static let shared: QDProgress = QDProgress()
    let view: UIView = UINib(nibName: "QDProgress", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    var count: Int = 0

    func show() {
        if 0 == count {
            self.add()
        }
        count += 1
    }
    
    func hide() {
        count -= 1
        if 0 >= count {
            self.reset()
        }
    }
    
    func reset() {
        count = 0
        self.remove()
    }
    
    private func add() {
        if let keyWindow = GlobalFunctions.shared.getKeyWindow() {
            self.view.frame = keyWindow.frame
            keyWindow.addSubview(self.view)
        }
    }
    
    private func remove() {
        UIView.animate(withDuration: 0.5, animations: {
            self.view.alpha = 0.0
        }) { (isFinish) in
            self.view.removeFromSuperview()
            self.view.alpha = 1.0
        }
    }
}

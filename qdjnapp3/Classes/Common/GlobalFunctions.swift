//
//  GlobalFunctions.swift
//  APIClient
//
//  Created by qdillion on 2017. 6. 19..
//  Copyright © 2017년 toplogic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

public class GlobalFunctions {
    //=============================================================================================//
    // MARK:- Singleton 설정
    static let shared = GlobalFunctions()
    
    //=============================================================================================//
    // MARK:- main keywindow, rootVC 가져오기
    func getKeyWindow() -> UIWindow? {
        return UIApplication.shared.keyWindow
    }
    
    func getRootVC() -> UIViewController? {
        return self.getKeyWindow()?.rootViewController
    }
    
    //=============================================================================================//
    // MARK:- http 통신 (Alamofire) 관련
    /// http 통신 타임아웃 설정
    let timeout: Double = 10
    
    /// - parameter isProgress : 프로그래스 사용 유무 -> on:true, off:false
    /// - parameter isAutoErrorMessage : 에러시 자동으로 alert 처리 유무 -> 자동: treu, 수동으로 직접처리 : false
    func request(_ url: String, method: HTTPMethod, parameters: Dictionary<String,Any>?, isProgress:Bool, isAutoErrorMessage:Bool, success:@escaping (JSON) -> Void, failure:@escaping (Error?) -> Void) {
        if isProgress {
            QDProgress.shared.show()
        }
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = self.timeout
        
        manager.request(url, method: method, parameters: parameters).responseJSON { (responseObject) in
            
            // Log 출력
            // 서버에서 보내주는 Unicode를 Log.out에서 한글로 볼수 있게 변환
            var strResponse: Any? = nil
            if let data  = responseObject.description.data(using: String.Encoding.utf8) {
                if let str: String = String(data: data, encoding: String.Encoding.nonLossyASCII) {
                    strResponse = str
                }
            }
            
            Log.out("\n\n============================================================\n<URL:\(method.rawValue)> \(url)\n============================================================\n<Parameters>\n\(parameters ?? ["No":"Parameters"])\n\n<Response>\n\(strResponse ?? responseObject)\n========================= END ==============================\n============================================================\n\n")
            
            // 통신 성공, 실패 처리
            if responseObject.result.isSuccess, let value = responseObject.result.value {
                // success request
                success(JSON(value))
            }
            else {
                // failure request
                failure(responseObject.result.error)
                
                if isAutoErrorMessage {
                    self.showSimpleAlert("네트워크 오류", message: "잠시 후 다시 시도해주세요.")
                }
            }
            
            if isProgress {
                QDProgress.shared.hide()
            }
        }
    }

    //=============================================================================================//
    // MARK:- Simple Alert 관련
    func showSimpleAlert(_ message: String?) {
        self.showSimpleAlert(nil, message: message, cancelButtonText: nil, okButtonText: "확인", handler: nil, completion: nil)
    }
    
    func showSimpleAlert(_ message: String?, handler: ((_ action: UIAlertAction, _ isOkBtn: Bool) -> Void)?, completion: (() -> Void)?) {
        self.showSimpleAlert(nil, message: message, cancelButtonText: nil, okButtonText: "확인", handler: handler, completion: completion)
    }
    
    func showSimpleAlert(_ message: String?, okButtonText: String) {
        self.showSimpleAlert(nil, message: message, cancelButtonText: nil, okButtonText: okButtonText, handler: nil, completion: nil)
    }
    
    func showSimpleAlert(_ title: String?, message: String) {
        self.showSimpleAlert(title, message: message, cancelButtonText: nil, okButtonText: "확인", handler: nil, completion: nil)
    }
    
    func showSimpleAlert(_ title: String?, message: String?, handler: ((_ action: UIAlertAction, _ isOkBtn: Bool) -> Void)?, completion: (() -> Void)?) {
        self.showSimpleAlert(title, message: message, cancelButtonText: nil, okButtonText: "확인", handler: handler, completion: completion)
    }
    
    func showSimpleAlert(_ title: String?, message: String, okButtonText: String) {
        self.showSimpleAlert(title, message: message, cancelButtonText: nil, okButtonText: okButtonText, handler: nil, completion: nil)
    }
    
    func showSimpleAlert(_ title: String?, message: String?, cancelButtonText: String?, okButtonText: String, handler: ((_ action: UIAlertAction, _ isOkBtn: Bool) -> Void)?, completion: (() -> Void)?) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // cancel 버튼
        if let text: String = cancelButtonText {
            let btn: UIAlertAction = UIAlertAction(title: text, style: .cancel, handler: { (action) in
                if let handler = handler {
                    handler(action, false)
                }
            })
            alert.addAction(btn)
        }
        
        // ok 버튼
        let btn: UIAlertAction = UIAlertAction(title: okButtonText, style: .default, handler: { (action) in
            if let handler = handler {
                handler(action, true)
            }
        })
        alert.addAction(btn)
        
        // 버튼 text 색상
        alert.view.tintColor = UIColor.alertColor
        
        // show
        if let vc = self.getRootVC() {
            vc.present(alert, animated: true, completion: {
                if let completion = completion {
                    completion()
                }
            })
        }
        else {
            // 문제 발생
        }
    }
}


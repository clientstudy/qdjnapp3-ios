//
//  Log.swift
//  qdjnapp3
//
//  Created by qdillion on 2018. 2. 6..
//  Copyright © 2018년 qdillion. All rights reserved.
//

import Foundation

//=============================================================================================//
// MARK:- LOG 관리
class Log {
    class func out(_ obejct: Any) {
        guard GlobalVariables.shared.server != .live else {return}
        
        print(obejct)
    }
    
    class func outDump(_ obejct: Any) {
        guard GlobalVariables.shared.server != .live else {return}
        
        dump(obejct)
    }
}
